module Bloginator exposing (content, sidebar)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html exposing (Html)
import List
import Mark
import Mark.Error
import Ui as Ui


content markup =
    -- Compiles markup with the document style
    case Mark.compile document markup of
        Mark.Success result ->
            result

        Mark.Almost { result, errors } ->
            column []
                [ el [] (text "twas errors")
                , column []
                    [ text <|
                        String.join "\n" <|
                            List.map Mark.Error.toString errors
                    ]
                , textColumn [] [ result ]
                ]

        Mark.Failure error ->
            textColumn [ centerX, centerY ]
                [ text "twas' bad error"
                , column []
                    [ text <|
                        String.join "\n" <|
                            List.map Mark.Error.toString error
                    ]
                ]


sidebar markup =
    -- Compiles markup with the sidebar style
    case Mark.compile navigation markup of
        Mark.Success result ->
            result

        Mark.Almost { result, errors } ->
            column []
                [ el [] (text "twas errors")
                , column []
                    [ text <|
                        String.join "\n" <|
                            List.map Mark.Error.toString errors
                    ]
                , textColumn [] [ result ]
                ]

        Mark.Failure error ->
            textColumn [ centerX, centerY ]
                [ text "twas' bad error"
                , column []
                    [ text <|
                        String.join "\n" <|
                            List.map Mark.Error.toString error
                    ]
                ]



{- Assemble Document -}


document =
    Mark.document
        (textColumn [ width (fill |> minimum 200) ])
    <|
        Mark.manyOf
            [ makeHeader
            , makeSubHeader
            , makeImage
            , Mark.map
                (paragraph Ui.regularTextAttributes)
                inlineMarkup
            ]


navigation =
    Mark.document
        (column [ width fill ])
    <|
        Mark.manyOf
            [ makeHeaderSideButton
            , makeSubheaderSideButton
            , ignoreImage
            , Mark.map Ui.noElement inlineMarkup
            ]



{- Handle Text -}


inlineMarkup =
    Mark.text styledTextToElement


styledTextToElement styles string =
    -- applies styles to in-line text
    let
        isBold bold =
            if bold then
                Font.semiBold

            else
                above none

        isItalic italic =
            if italic then
                Font.italic

            else
                above none

        isStrike strike =
            if strike then
                Font.strike

            else
                above none
    in
    if styles.bold || styles.italic || styles.strike then
        Ui.regularText
            -- Checks if respective style is true and adds that to the list if
            -- not then functions return a attribute that doesn't do anything.
            [ isBold styles.bold
            , isItalic styles.italic
            , isStrike styles.strike
            ]
            string

    else
        -- Ui.regularText [] string
        el [] (text string)



{- Handling Blocks -}


makeHeader : Mark.Block (Element msg)
makeHeader =
    Mark.block "Header"
        (\header ->
            Ui.header header
        )
        inlineMarkup


makeSubHeader : Mark.Block (Element msg)
makeSubHeader =
    Mark.block "Subheader"
        (\subheader -> Ui.subHeader subheader)
        inlineMarkup


makeImage : Mark.Block (Element msg)
makeImage =
    Mark.record "Image"
        (\src alignment description ->
            Ui.regularImage
                { src = src
                , alignment = alignment
                , description = description
                }
        )
        |> Mark.field "src" Mark.string
        |> Mark.field "alignment" Mark.string
        |> Mark.field "description" Mark.string
        |> Mark.toBlock


makeHeaderSideButton : Mark.Block (Element msg)
makeHeaderSideButton =
    Mark.block "Header"
        (\header -> Ui.sideButton header)
        inlineMarkup


makeSubheaderSideButton : Mark.Block (Element msg)
makeSubheaderSideButton =
    Mark.block "Subheader"
        (\header -> Ui.sideButton header)
        inlineMarkup


ignoreImage : Mark.Block (Element msg)
ignoreImage =
    Mark.record "Image"
        (\_ _ _ -> none)
        |> Mark.field "src" Mark.string
        |> Mark.field "alignment" Mark.string
        |> Mark.field "description" Mark.string
        |> Mark.toBlock
