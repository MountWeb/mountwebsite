module Content exposing (home, secondTest, test)


home : String
home =
    """
|> Header
    Purpose

Mount Web's primary focus is to supply Mount Gambier and the surrounding
region's businesses and individuals with modern, fast websites by competing
fairly with current competition. Our technology should be maintainable foremost
as well as open for people to utilize to help foster a healthy technology
ecosystem for the region.

|> Header
    Principles

We believe a good company is a principled company, therefore all actions done on
behalf of Mount Web by employees are expected to adhere to these simple human
fundamentals. Not by objective but my constraint.

|> Subheader
    Integrity

Any principle or value we have is meaningless without the integrity to uphold
them; our integrity is integral. We do not expend our principles for a easy or
fast route.

|> Subheader
    Honesty

Truth is a cornerstone to all. No relationship or partnership can truly succeed
without it. Even when these truths are painful or inconvenient we will abide by
the spirit of the truth; we do not hide falsehoods and lies within language that
is technically true or otherwise misleading.

|> Subheader
    Decency

Colleague, customer, friend, or competitor are treated with dignity at Mount
Web.

|> Header
    Values

While principles are universally human, our values are of personal importance.
They are objectives rather than constraints, they can come into tension with one
another as any of them, when taken to their extreme, are either simply
impractical or just plain silly. Absolute adherence to a value should never rise
above simple common sense.
  
|> Subheader
    Candour

We value a blunt sentience, being candid when it's hard and avoiding 
any vales of cloaks obscuring our experiences and opinions. 

|> Subheader
    Curiosity

Learning never stops once you leave a classroom. We are unafraid to learn 
something new, whether that be a new technology, a strange hiccup in normal 
behaviour, or a novel customer use case.

|> Subheader
    Empathy

For us to suit the needs of others we must be able to walk in their boots and
understand, truly, what they need. This doesn't just extend to our product, to
fill out a design check-list, it guides our interactions with colleagues and
customers, as the classic saying goes, treat others as we would like to be
treated.

|> Subheader
    Humour

While we conduct serious business, we don't take ourselves with the same level
of stoicism. We enjoy time spent with the company of our colleagues and 
customers. A day without laughter is a day without life.

|> Subheader
    Prudence

There isn't enough time in a day to waste on the in-between, whether that be 
meeting up with customers at convenient times or giving clear time-lines with
flexible releases. 

|> Subheader
    Resilience

We believe in the words of the late mathematician Piet Hein: "problems worthy 
of attack prove their worth by fighting back." When problems arise we rise to 
meet it, pushing through setbacks and disappointment.

|> Image
    src = http://placekitten.com/200/400
    alignment = right
    description = cute kot.


|> Subheader
    Responsibility

We aren't just here to sell a product, we seek ways to actively help. Our duty
is larger than just obligations and because of that we balance our
professional, personal and familial responsibilities and we honour those who
do the same.

|> Subheader
    Simplicity

We believe that often complexity is unnecessary and time blinds us to what
matters. We keep the nitty-gritty and strange abstractions out of the way and
make sure our definitions are clear.

|> Subheader
    Transparency

We believe that secrets are often corrosive and that we work best when we have
a clearer picture. We err on the side of transparency and communication: every
employee knows the state of the business and our customers knows their
standing.

|> Subheader
    Versatility

We believe a designer should be able to develop and a admin should be able to
wire-frame a site from scratch. Whist we naturally specialise, this
versatility and ability to adapt is important for potential constrained
resources and efficiency.

    """


test : String
test =
    """
|> Header
    Now for something completely different

WOW! Hopefully this means a button is working

|> Header
    Principles

We believe a good company is a principled company, therefore all actions done on
behalf of Mount Web by employees are expected to adhere to these simple human
fundamentals. Not by objective but my constraint.

|> Subheader
    Integrity

Any principle or value we have is meaningless without the integrity to uphold
them; our integrity is integral. We do not expend our principles for a easy or
fast route.

|> Subheader
    Honesty

Truth is a cornerstone to all. No relationship or partnership can truly succeed
without it. Even when these truths are painful or inconvenient we will abide by
the spirit of the truth; we do not hide falsehoods and lies within language that
is technically true or otherwise misleading.

|> Subheader
    Decency

Colleague, customer, friend, or competitor are treated with dignity at Mount
Web.
    """


secondTest : String
secondTest =
    """
|> Header
    Money

100 gecs is art god damn it!
    """
