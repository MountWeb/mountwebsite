module Ui exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input



{- Constants -}


colours :
    { blue1 : Element.Color
    , blue2 : Element.Color
    , blue3 : Element.Color
    , blue4 : Element.Color
    , gray1 : Element.Color
    , gray2 : Element.Color
    , gray3 : Element.Color
    , white : Element.Color
    }
colours =
    { blue1 = rgb255 0 29 108
    , blue2 = rgb255 0 45 156
    , blue3 = rgb255 0 67 206
    , blue4 = rgb255 15 98 254
    , gray1 = rgb255 244 244 244
    , gray2 = rgb255 198 198 198
    , gray3 = rgba 0 0 0 0.75
    , white = rgb255 255 255 255
    }


edges : { top : Int, bottom : Int, left : Int, right : Int }
edges =
    { top = 0, bottom = 0, left = 0, right = 0 }



{- Font Matter -}


baseSize : Float
baseSize =
    16


fontScale : Float -> Int
fontScale n =
    round (baseSize * n)


fontSize : Float -> Attribute msg
fontSize n =
    Font.size <| fontScale n


regularFont : Attribute msg
regularFont =
    Font.family
        [ Font.external
            { name = "IBM Plex Sans"
            , url = "https://fonts.googleapis.com/css?family=IBM+Plex+Sans"
            }
        , Font.sansSerif
        ]


monospaceFont : Attribute msg
monospaceFont =
    Font.family
        [ Font.external
            { name = "Major Mono Display" -- "IBM Plex Mono"
            , url = "https://fonts.googleapis.com/css2?family=Major+Mono+Display"

            --"https://fonts.googleapis.com/css2?family=IBM+Plex+Mono"
            }
        , Font.monospace
        ]



{- Page Facing Components -}


header : List (Element msg) -> Element msg
header content =
    column
        [ regularFont
        , Font.semiBold
        , Font.size <| fontScale 2
        , paddingEach { edges | top = 16, bottom = 8 }
        ]
        content


subHeader : List (Element msg) -> Element msg
subHeader content =
    column
        [ regularFont
        , Font.size <| fontScale 1.5
        , Font.semiBold
        , paddingEach { edges | top = 16, bottom = 8 }
        ]
        content


regularText : List (Attribute msg) -> String -> Element msg
regularText attributes content =
    el
        (List.concat
            [ attributes
            , [ regularFont
              ]
            ]
        )
        (text content)


regularTextAttributes : List (Attribute msg)
regularTextAttributes =
    [ Font.size <| fontScale 1
    , Font.regular
    , paddingEach { edges | bottom = 8 }
    ]


regularImage :
    { src : String, alignment : String, description : String }
    -> Element msg
regularImage { src, alignment, description } =
    if alignment == "right" then
        image
            [ alignRight
            , paddingXY 8 4
            ]
            { src = src, description = description }

    else if alignment == "left" then
        image
            [ alignLeft
            , paddingXY 8 4
            ]
            { src = src, description = description }

    else
        image [ padding 16 ] { src = src, description = description }


noElement : List (Element msg) -> Element msg
noElement _ =
    el [] none



{- View Components -}


namePlate : String -> msg -> Element.Color -> Element msg
namePlate name change colour =
    Input.button
        [ width <| fillPortion 1
        , height fill
        , paddingXY 0 8
        , Border.shadow
            { offset = ( -5, 0 )
            , size = 5
            , blur = 20
            , color = rgba255 4 4 4 0.25
            }
        , Background.color colour
        , clip
        ]
        { onPress = Just change
        , label =
            column
                [ centerX
                , height fill
                , Font.color colours.white
                , Font.size 64
                , monospaceFont
                ]
                (if name == "Home" then
                    [ text "h"
                    , text "o"
                    , text "m"
                    , text "e"
                    ]

                 else if name == "Products" then
                    [ text "p"
                    , text "r"
                    , text "d"
                    , text "u"
                    , text "c"
                    , text "t"
                    ]

                 else if name == "Gallery" then
                    [ text "g"
                    , text "a"
                    , text "l"
                    , text "l"
                    , text "e"
                    , text "r"
                    , text "y"
                    ]

                 else if name == "About Us" then
                    [ text "a"
                    , text "b"
                    , text "o"
                    , text "u"
                    , text "t"
                    , text " "
                    , text "u"
                    , text "s"
                    ]

                 else if name == "Navigation" then
                    [ text "n"
                    , text "a"
                    , text "v"
                    , text "i"
                    , text "g"
                    , text "a"
                    , text "t"
                    , text "i"
                    , text "o"
                    , text "n"
                    ]

                 else
                    [ none ]
                )
        }


sidePanel : Element msg -> Element msg
sidePanel content =
    column
        [ spacing 5
        , height fill
        , width <| fillPortion 3
        , scrollbarY
        , Border.roundEach
            { topLeft = 32
            , topRight = 0
            , bottomLeft = 32
            , bottomRight = 0
            }
        , Background.color colours.gray1
        ]
        [ content ]


sideButton : List (Element msg) -> Element msg
sideButton item =
    Input.button
        [ height <| px 48
        , width fill
        , Border.widthEach { edges | left = 6 }
        , Border.color <| rgba 0 0 0 0
        , mouseOver
            [ Background.color colours.gray2
            , Border.color colours.blue1
            ]
        , Font.size <| fontScale 1.5
        ]
        { onPress = Nothing
        , label =
            el [] (row [ centerY, paddingXY 8 0, Font.regular ] item)
        }


contentColumn : Element msg -> Element msg
contentColumn content =
    column
        [ width <| fillPortion 8, paddingXY 20 0 ]
        [ content ]


contentPanel : Element msg -> Element msg
contentPanel content =
    column
        [ height fill
        , width <| fillPortion 9
        , scrollbarY
        , Background.color colours.white
        ]
        [ el [ width <| fillPortion 1 ] none
        , contentColumn content
        , el [ width <| fillPortion 1 ] none
        ]


portraitContentPanel : Element msg -> Element msg
portraitContentPanel content =
    column
        [ spacing 10
        , height fill
        , width <| fillPortion 6
        , Border.roundEach
            { topLeft = 32
            , topRight = 0
            , bottomLeft = 32
            , bottomRight = 0
            }
        , scrollbarY
        , Background.color colours.white
        ]
        [ el [ width <| fillPortion 2 ] none
        , contentColumn content
        , el [ width <| fillPortion 2 ] none
        ]



{- Responsive Components -}


type
    Orientation
    -- The view-ports orientation
    = Portrait
    | Landscape


classifyOrientation : { window | height : Int, width : Int } -> Orientation
classifyOrientation window =
    if window.width < window.height then
        Portrait

    else
        Landscape
