module Home exposing (..)

import Bloginator as Compile
import Browser
import Browser.Dom as Dom
import Browser.Events as Window
import Content
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Event
import Html exposing (Html)
import Http
import Task
import Ui as Ui


main =
    Browser.element
        { init = initalValues
        , update = updateValues
        , view = layout
        , subscriptions = subscriptions
        }



{- Main -}


type alias Model =
    { orientation : Ui.Orientation
    , windowWidth : Int
    , content : Element Change
    , sidebar : Element Change
    , currentPage : Page
    , navOpen : Bool
    }


initalValues : () -> ( Model, Cmd Change )
initalValues _ =
    ( { orientation = Ui.Landscape
      , windowWidth = 540
      , content = Compile.content Content.home
      , sidebar = Compile.sidebar Content.home
      , currentPage = Home
      , navOpen = False
      }
    , Task.perform GotViewport Dom.getViewport
    )



{- Update -}


type Change
    = GotViewport Dom.Viewport
    | WindowSize ( Int, Int )
    | SetPage Page
    | ToggleNav


type Page
    = Home
    | Products
    | Gallery
    | AboutUs


updateValues : Change -> Model -> ( Model, Cmd Change )
updateValues change model =
    ( case change of
        GotViewport viewport ->
            { model
                | orientation =
                    Ui.classifyOrientation
                        { width = round viewport.viewport.width
                        , height = round viewport.viewport.height
                        }
                , windowWidth = round viewport.viewport.width
            }

        WindowSize ( width, height ) ->
            { model
                | orientation =
                    Ui.classifyOrientation
                        { width = width
                        , height = height
                        }
                , windowWidth = width
            }

        SetPage page ->
            case page of
                Home ->
                    { model
                        | content = Compile.content Content.home
                        , sidebar = Compile.sidebar Content.home
                        , currentPage = Home
                        , navOpen = False
                    }

                Products ->
                    { model
                        | content = Compile.content Content.test
                        , sidebar = Compile.sidebar Content.test
                        , currentPage = Products
                        , navOpen = False
                    }

                Gallery ->
                    { model
                        | content = Compile.content Content.secondTest
                        , sidebar = Compile.sidebar Content.secondTest
                        , currentPage = Gallery
                        , navOpen = False
                    }

                AboutUs ->
                    { model
                        | content = Compile.content Content.home
                        , sidebar = Compile.sidebar Content.home
                        , currentPage = AboutUs
                        , navOpen = False
                    }

        ToggleNav ->
            if model.navOpen then
                { model | navOpen = False }

            else
                { model | navOpen = True }
    , Cmd.none
    )



{- Subscriptions -}


subscriptions : Model -> Sub Change
subscriptions _ =
    Window.onResize (\w h -> WindowSize ( w, h ))



{- Layout -}


layout : Model -> Html Change
layout model =
    Element.layout
        (case model.navOpen of
            False ->
                [ Background.color Ui.colours.blue1, width fill, height fill ]

            True ->
                [ Background.color Ui.colours.blue1
                , width fill
                , height fill
                , inFront
                    (row
                        [ width fill, height fill ]
                        [ el
                            [ width <| fillPortion 4
                            , height fill
                            , Background.color Ui.colours.gray3
                            , Event.onClick ToggleNav
                            ]
                            none
                        , Ui.namePlate
                            "Home"
                            (SetPage Home)
                            Ui.colours.blue1
                        , Ui.namePlate
                            "Products"
                            (SetPage Products)
                            Ui.colours.blue2
                        , Ui.namePlate
                            "Gallery"
                            (SetPage Gallery)
                            Ui.colours.blue3
                        , Ui.namePlate
                            "About Us"
                            (SetPage AboutUs)
                            Ui.colours.blue4
                        ]
                    )
                ]
        )
    <|
        view model


view : Model -> Element Change
view model =
    case model.currentPage of
        Home ->
            case model.orientation of
                Ui.Landscape ->
                    landscapeLayout (SetPage Home) model.content model.sidebar

                Ui.Portrait ->
                    portraitLayout "Home" (SetPage Home) model.content

        Products ->
            case model.orientation of
                Ui.Landscape ->
                    landscapeLayout (SetPage Products) model.content model.sidebar

                Ui.Portrait ->
                    portraitLayout "Products" (SetPage Products) model.content

        Gallery ->
            case model.orientation of
                Ui.Landscape ->
                    landscapeLayout (SetPage Gallery) model.content model.sidebar

                Ui.Portrait ->
                    portraitLayout "Gallery" (SetPage Gallery) model.content

        AboutUs ->
            case model.orientation of
                Ui.Landscape ->
                    landscapeLayout (SetPage AboutUs) model.content model.sidebar

                Ui.Portrait ->
                    portraitLayout "About Us" (SetPage AboutUs) model.content


landscapeLayout : Change -> Element Change -> Element Change -> Element Change
landscapeLayout change content sidebar =
    case change of
        SetPage page ->
            case page of
                Home ->
                    row
                        [ width fill
                        , height fill
                        ]
                        [ Ui.namePlate
                            "Home"
                            (SetPage Home)
                            Ui.colours.blue1
                        , Ui.sidePanel sidebar
                        , Ui.contentPanel content
                        , Ui.namePlate
                            "Products"
                            (SetPage Products)
                            Ui.colours.blue2
                        , Ui.namePlate
                            "Gallery"
                            (SetPage Gallery)
                            Ui.colours.blue3
                        , Ui.namePlate
                            "About Us"
                            (SetPage AboutUs)
                            Ui.colours.blue4
                        ]

                Products ->
                    row
                        [ width fill
                        , height fill
                        ]
                        [ Ui.namePlate
                            "Products"
                            (SetPage Products)
                            Ui.colours.blue1
                        , Ui.sidePanel sidebar
                        , Ui.contentPanel content
                        , Ui.namePlate
                            "Gallery"
                            (SetPage Gallery)
                            Ui.colours.blue2
                        , Ui.namePlate
                            "About Us"
                            (SetPage AboutUs)
                            Ui.colours.blue3
                        , Ui.namePlate
                            "Home"
                            (SetPage Home)
                            Ui.colours.blue4
                        ]

                Gallery ->
                    row
                        [ width fill
                        , height fill
                        ]
                        [ Ui.namePlate
                            "Gallery"
                            (SetPage Gallery)
                            Ui.colours.blue1
                        , Ui.sidePanel sidebar
                        , Ui.contentPanel content
                        , Ui.namePlate
                            "About Us"
                            (SetPage AboutUs)
                            Ui.colours.blue2
                        , Ui.namePlate
                            "Home"
                            (SetPage Home)
                            Ui.colours.blue3
                        , Ui.namePlate
                            "Products"
                            (SetPage Products)
                            Ui.colours.blue4
                        ]

                AboutUs ->
                    row
                        [ width fill
                        , height fill
                        ]
                        [ Ui.namePlate
                            "About Us"
                            (SetPage AboutUs)
                            Ui.colours.blue1
                        , Ui.sidePanel sidebar
                        , Ui.contentPanel content
                        , Ui.namePlate
                            "Home"
                            (SetPage Home)
                            Ui.colours.blue2
                        , Ui.namePlate
                            "Products"
                            (SetPage Products)
                            Ui.colours.blue3
                        , Ui.namePlate
                            "Gallery"
                            (SetPage Gallery)
                            Ui.colours.blue4
                        ]

        _ ->
            none


portraitLayout : String -> Change -> Element Change -> Element Change
portraitLayout primary change content =
    row
        [ width fill
        , height fill
        ]
        [ Ui.namePlate primary change Ui.colours.blue1
        , portraitContentPanel content
        , Ui.namePlate "Navigation" ToggleNav Ui.colours.blue2
        ]


portraitContentPanel : Element Change -> Element Change
portraitContentPanel content =
    column
        [ spacing 10
        , height fill
        , width <| fillPortion 6
        , Border.roundEach
            { topLeft = 32
            , topRight = 0
            , bottomLeft = 32
            , bottomRight = 0
            }
        , scrollbarY
        , Background.color Ui.colours.white
        ]
        [ el [ width <| fillPortion 2 ] none
        , Ui.contentColumn content
        , el [ width <| fillPortion 2 ] none
        ]
